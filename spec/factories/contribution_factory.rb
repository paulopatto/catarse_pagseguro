#encoding: utf-8
FactoryGirl.define do
  factory :contribution, class: OpenStruct do |contribution|
    contribution.id   4
    contribution.project_id 1
    contribution.user_id    2
    contribution.reward_id nil
    contribution.value "20.0"
    contribution.confirmed_at nil
    contribution.created_at "2014-05-09T17:00:07.866-03:00"
    contribution.updated_at "2014-05-09T17:00:39.608-03:00"
    contribution.anonymous  true
    contribution.key        "9b7750244209fa234176eafe4b159c2a"
    contribution.credits    false
    contribution.notified_finish false
    contribution.payment_method nil
    contribution.payment_token  nil
    contribution.payment_id     nil
    contribution.payer_name            "Fulano de Tao"
    contribution.payer_email           "contributor@mailinator.com"
    contribution.payer_document        "778.383.297-01"
    contribution.address_street        "Av. Brigadeiro Faria Lima"
    contribution.address_number        "3477"
    contribution.address_complement    "18º Andar"
    contribution.address_neighbourhood "Itaim Bibi"
    contribution.address_zip_code      "04538-133"
    contribution.address_city          "São Paulo"
    contribution.address_state         "SP"
    contribution.address_phone_number  "(11) 2395-8400"
    contribution.payment_choice      nil
    contribution.payment_service_fee nil
    contribution.state               "pending"
    contribution.referal_link        ""
  end
end
