#encoding: utf-8

FactoryGirl.define do
  factory :address1, class: OpenStruct do |address|
    address.street 'Avenida Brigadeiro Faria Lima'
    address.number 1834
    address.complement '5o Andar'
    address.district 'Jardim Paulistano'
    address.postalCode 01452002
    address.city 'São Paulo'
    address.state 'São Paulo'
  end

  factory :address2, class: OpenStruct do |address|
    address.street 'Rua dos Pinheiro'
    address.number 1001
    address.district 'Pinheiros'
    address.postalCode 01452000
    address.city 'São Paulo'
    address.state 'São Paulo'
  end
end
