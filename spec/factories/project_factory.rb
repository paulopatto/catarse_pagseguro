FactoryGirl.define do
  factory :project, class: OpenStruct do |project|
    project.id 1
    project.name "Project"
  end
end
