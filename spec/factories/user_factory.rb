#encoding: utf-8
FactoryGirl.define do
  factory :user, class: OpenStruct do |user|
    user.name 'Usuario de Teste'
    user.email 'c34259678907504288333@sandbox.pagseguro.com.br'
  end
end
