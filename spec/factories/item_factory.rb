FactoryGirl.define do
  factory :item, class: OpenStruct do |item|
    item.description "Sample Book"
    item.amount 1.99
    item.weigth 0.3
    item.quantity 1
  end
end
