require 'spec_helper'

describe CatarsePagseguro::PagseguroController do
  subject{ CatarsePagseguro::PagseguroController }

  let(:user)              { FactoryGirl.build(:user) }
  let(:project)           { FactoryGirl.build(:project) }
  let(:contribution)      { FactoryGirl.build(:contribution, user: user, project: project) }
  let(:id_transaction)    { "9b7750244209fa234176eafe4b159c2a" }
  let(:notification_code) { "6BFC0BB6646448111468AF865B3B94A6" }

  describe 'POST payment/pagseguro/notifications => #create_notification' do
    pending
  end

  describe 'POST payment/pagseguro/:id/response => pagseguro_response' do
    pending
  end

  describe 'GET payment/pagseguro/:id/review => pagseguro_review' do
    pending
  end

  describe "POST #pay" do
    before do
      PaymentEngines.stub(:find_payment).with(hash_including(:id)).and_return(contribution)
    end

    context 'when pass valid key' do
      it 'receive checkout url' do
        VCR.use_cassette('paypal_checkout') do
          post :pay, id: contribution.id, cpf: '55257342812', use_route: :catarse_pagseguro
        end

        response_hash= JSON(response.body).to_hash
        expect(response_hash).to have_key('url')
      end
    end
  end
end
