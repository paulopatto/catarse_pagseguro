ENV["RAILS_ENV"] ||= 'test'
require File.expand_path("../../test/dummy/config/environment", __FILE__)
require 'rspec/rails'
require 'rspec/autorun'
require 'factory_girl'
require 'vcr'
require 'webmock/rspec'

ENGINE_RAILS_ROOT=File.join(File.expand_path('../../', __FILE__))
Dir[File.join(ENGINE_RAILS_ROOT, 'spec/support/**/*.rb')].each {|f| require f }

WebMock.disable_net_connect!
FactoryGirl.find_definitions
VCR.configure do |config|
  config.default_cassette_options = { :match_requests_on => [:uri, :method, :body, :headers] }
  config.cassette_library_dir = 'spec/fixtures/cassettes'
  config.hook_into :webmock
  config.configure_rspec_metadata!
end

RSpec.configure do |config|
  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  config.use_transactional_fixtures = false

  # If true, the base class of anonymous controllers will be inferred
  # automatically. This will be the default behavior in future versions of
  # rspec-rails.
  config.infer_base_class_for_anonymous_controllers = false

  config.include FactoryGirl::Syntax::Methods

  # Include Engine routes (needed for Controller specs)
  config.include CatarsePagseguro::Engine.routes.url_helpers

  config.before(:each) do
    PaymentEngines.stub(:configuration).and_return({})
  end
end
