$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "catarse_pagseguro/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "catarse_pagseguro"
  s.version     = CatarsePagseguro::VERSION
  s.authors     = ["Paulo Patto"]
  s.email       = ["paulopatto@gmail.coml"]
  s.homepage    = "http://ajudedoando.com.br"
  s.summary     = "Suporte ao gateway de pagamentos Pagseguro"
  s.description = "Suporte ao Pagseguro no Catarse"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = s.files.grep(%r{^(test|spec|features)/})

  s.add_dependency "rails", "~> 4.0"
  s.add_dependency "pagseguro-oficial"

  s.add_development_dependency "rspec-rails"
  s.add_development_dependency "sqlite3"
  s.add_development_dependency "factory_girl_rails"
  s.add_development_dependency "vcr"
  s.add_development_dependency "webmock"
  s.add_development_dependency "database_cleaner"
end
