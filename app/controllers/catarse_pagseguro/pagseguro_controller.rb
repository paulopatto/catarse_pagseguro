class CatarsePagseguro::PagseguroController < ApplicationController
  attr_accessor :contribution

  skip_before_filter :force_http
  layout false

  # POST payment/pagseguro/notifications
  def create_notification
    @contribution = PaymentEngines.find_payment(key: params[:id_transaction])
  end

  # POST payment/pagseguro/
  # @SEE: http://goo.gl/oEHAh
  #
  # => PARAM id_transaction
  def checkout
    @contribution = PaymentEngines.find_payment(id: params[:id])
    @payment      = gateway.checkout

    @payment.reference = @contribution.key

    @payment.sender = build_sender
    @payment.items << get_item

    @payment.redirect_url = "#{request.scheme}://#{request.host_with_port}#{notification_pagseguro_path(@contribution.id)}"

    #TODO: remove this
    Rails.logger.info "************************************************************"
    Rails.logger.info "* EMAIL #{@payment.email}"
    Rails.logger.info "* TOKEN #{@payment.token}"
    Rails.logger.info "************************************************************"
    Rails.logger.info PagSeguro::PaymentRequest::Serializer.new(@payment).to_params
    Rails.logger.info @payment.sender.to_json
    Rails.logger.info @payment.items.to_json
    Rails.logger.info PagSeguro.uris

    response = @payment.register

    @contribution.update_attribute :referal_link, response.url

    if response.success?
      redirect_to @contribution.referal_link
    else
      Rails.logger.error "**** Payment with PagSeguro as returned #{response.errors.to_a} ****"

      flash[:failure] = "#{t('projects.contributions.checkout.pagseguro_error')} # #{response.errors.to_a}"
      redirect_to main_app.new_project_contribution_path(contribution.project)
    end
  end

  def review
    #Action required by catarse
  end

  # POST payment/pagseguro/notify
  # @SEE: http://goo.gl/DR83l
  #
  # => PARAM notificationCode:string
  # => PARAM notificationType:string
  def notification
    @contribution = Contribution.find_by id: params[:id]
    @contribution.update_attributes(payment_token: params[:token])
    flash[:success] = t('projects.contributions.checkout.success')
    redirect_to main_app.new_project_contribution_path(contribution.project)
  end

  protected
  def gateway
    @gateway ||= CatarsePagseguro::Gateway.new
  end

  def build_sender
    @user = @contribution.user
    {name: @contribution.payer_name, email: @user.email, cpf: params[:cpf]}
  end

  def get_item
    { id: @contribution.project.id, description: @contribution.project.name, amount: @contribution.value.to_f }
  end
end
