CatarsePagseguro::Engine.routes.draw do
  resources :pagseguro, only: [], path: 'payment/pagseguro' do
    collection do
    end

    member do
      post :checkout
      get :review
      get :notification
    end
  end
end
