begin
  PaymentEngines.register(CatarsePagseguro::PaymentEngine.new)
rescue Exception => e
  puts "Error while registering payment engine: #{e}"
end
