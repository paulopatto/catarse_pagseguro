#encoding: UTF-8

require 'pagseguro-oficial'
require 'dotenv'

Dotenv.load

PagSeguro.configure do |config|
  config.email = ENV['PAGSEGURO_EMAIL']
  config.token = ENV['PAGSEGURO_TOKEN']
  config.environment = ENV['PAGSEGURO_ENVIRONMENT']
end

@payment = PagSeguro::PaymentRequest.new

#@payment.abandon_url        = 'http://127.0.0.1:9000/abandoned'
#@payment.redirect_url       = 'http://127.0.0.1:9000/redirect'
#@payment.notification_url   = 'http://127.0.0.1:9000/notify'

@sender = OpenStruct.new({
  name: 'Barack Obama',
  email: 'c34259678907504288333@sandbox.pagseguro.com.br',
  cpf: '78770730385',
  phone: {area_code: 11, number: 90345821}
})


@payment.items << { id: 0, description: 'Donativo', amount: 15.90 }
@payment.sender = @sender.to_h

puts "*" * 30
puts "\t [PARAMS]: #{PagSeguro.api_url('checkout')}"
puts PagSeguro::PaymentRequest::Serializer.new(@payment).to_params
puts "*" * 30

@response = @payment.register


puts "===Response==="
puts "\t#{@response.errors.to_a}"
puts @response.url
puts @response.code

# curl -iv -XPOST -H "Content-Type:application/xml" -H "Charset: ISO-8859-1" -d @spec/fixtures/ficha.xml -k "https://ws.sandbox.pagseguro.uol.com.br/v2/checkout" --insecure

=begin
PagSeguro.configure do |config|
  config.token = 'TOKEN' #ENV['PAGSEGURO_TOKEN']
  config.environment = :production #ENV['PAGSEGURO_ENVIRONMENT']
end

puts "*" * 30
puts "\t [PARAMS]: #{PagSeguro.api_url('checkout')}"
puts PagSeguro::PaymentRequest::Serializer.new(@payment).to_params
puts "*" * 30


@response = @payment.register
puts "===Response==="
puts @response.url
puts @response.code
puts @response.errors.to_a
=end

puts "========== FIM =============="
