module CatarsePagseguro
  class Gateway
    attr_reader :checkout, :notification

    def initialize(config = PaymentEngines.configuration)
      setup(config)
      self
    end

    def checkout
      @checkout
    end

    def notification
      @notification
    end

    private
    def setup(config)
      require 'pagseguro-oficial'

      token = config[:pagseguro_token]
      email = config[:pagseguro_email]

      #PagSeguro.configure do |pagseguro|
      #  pagseguro.token =       config[:pagseguro_token]       || ENV['PAGSEGURO_TOKEN'] ||
      #  pagseguro.email =       config[:pagsgeuro_email]       || ENV['PAGSEGURO_EMAIL'] ||
      #  pagseguro.environment = config[:pagseguro_environment] || ENV['PAGSEGURO_ENVIRONMENT'].try(:to_sym)
      #end

      @checkout     = PagSeguro::PaymentRequest.new(email: email, token: token)
      @notification = PagSeguro::Notification.new #(email: email, token: token)
    end
  end
end

#HACK
module PagSeguro
  def self.uris
    {production: {site: "https://sandbox.pagseguro.uol.com.br/v2", api: "https://ws.sandbox.pagseguro.uol.com.br/v2"}}
  end
end

