class CatarsePagseguro::PaymentEngine
  def name
    'PagSeguro'
  end

  def locale
    'pt-br'
  end

  def review_path(contribution)
    CatarsePagseguro::Engine.routes.url_helpers.review_pagseguro_path(contribution)
  end

  def can_do_refund?
    false
  end

  def direct_refund
    false
  end
end
